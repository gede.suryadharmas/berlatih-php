<?php

function tukar_besar_kecil($string){
    $tukar = strtolower($string)^strtoupper($string)^$string;
    echo "Kata asli: $string" . "<br>";
    echo "Tukar besar kecil: $tukar" . "<br>" . "<br>";
}

//kode di sini strtolower dan strtoupper
// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>