<?php
function tentukan_nilai($number){
    if ($number >= 85){
        $grade = "Sangar Baik";
    }else if ($number >= 70){
        $grade = "Baik";
    }else if ($number >= 60){
        $grade = "Cukup";
    }else if ($number < 60){
        $grade = "Kurang";
    }
    echo "Your Score: $number <br>";
    echo "Your Grade: $grade <br> <br>";
}

// Di dalam file tersebut buatlah function dengan nama tentukan_nilai yang menerima parameter berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
/* range pertama: 100 - 85 => sangat baik
    ranger kedua: 85 - 70 => Baik
    ranger ketiga: 70 - 60 => Cukup
    ranger keempat: 60 -  => Kurang
*/

//TEST CASES

echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>
